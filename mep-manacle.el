;;; mep-manacle.el --- An approach to controlling windows -*- lexical-binding: t -*-

;; Author: Magnus Therning
;; Maintainer: Magnus Therning
;; Version: 0
;; Package-Requires: ()
;; Homepage: none
;; Keywords: none


;; This file is not part of GNU Emacs

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; My attempt at dealing with windows that pop up.

;;; Code:

;;;###autoload
(defun mep-manacle-make-match-mode-function (modes)
  "Create a `display-buffer-alist' function matching on major mode.

The created function returns non-nil if the major mode is one of
MODES."
  (lambda (buffer-name _action)
    (with-current-buffer buffer-name (memq major-mode modes))))

;;;###autoload
(defun mep-manacle-make-match-derived-mode-function (modes)
  "Create a `display-buffer-alist' function matching on derived major mode.

The created function returns non-nil if the major mode is derived
from one of MODES."
  (lambda (buffer-name _action)
    (with-current-buffer buffer-name (apply #'derived-mode-p modes))))

(provide 'mep-manacle)
;;; mep-manacle.el ends here
